import { Dialog } from '@material-ui/core'
import { Icon, IconButton } from '@material-ui/core'
import { DialogTitle } from '@material-ui/core'
import { DialogContentText } from '@material-ui/core'
import { DialogContent } from '@material-ui/core'
import { TextField, Button, DialogActions, Grid, makeStyles, Table, TableBody, TableCell, TableHead, TableRow, Typography } from '@material-ui/core'
import React, { useEffect } from 'react'
import { useState } from 'react'
import { API_GET, API_POST } from '../../data/api'
import VisibilityIcon from '@material-ui/icons/Visibility';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        height: '70vh'
    }
}))

export default function Home() {
    const classes = useStyles()
    const [reminders, setReminders] = useState([])
    const [open, setOpen] = useState(false)
    const [reminderOpen, setReminderOpen] = useState(false)
    const [description, setDescription] = useState('')
    const [reload, setReload] = useState(false)
    const [reminder, setReminder] = useState(null)

    const getData = () => {
        API_GET('api/reminder').then(res => {
            setReminders(res.data.data)
        })
    }

    const viewReminder = (reminder) => {
        setReminderOpen(true)
        setReminder(reminder)
    }

    const submit = async () => {
        await API_POST({description}, '/api/reminder').then(res => {
            console.log(res)
            setReload(!reload)
            setOpen(false)
        })
    }

    useEffect(() => {
        getData()
    }, [reload])

    return (
        <Grid className={classes.root} container direction="column" alignItems="center" justify="center">
            <Grid item>
                <div style={{flexGrow:1}}>
                <Typography variant="h3">My Reminders</Typography>
                <IconButton onClick={() => setOpen(true)} ><Icon>add</Icon></IconButton>
                </div>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Reminder</TableCell>
                            <TableCell>Status</TableCell>
                            <TableCell>Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            reminders.map(reminder => 
                                <TableRow key={reminder.id}>
                                    <TableCell>{reminder.description}</TableCell>
                                    <TableCell>{reminder.status ? "Completed" : "Incomplete"}</TableCell>
                                    <TableCell><IconButton onClick={() => viewReminder(reminder)} ><VisibilityIcon/></IconButton></TableCell>
                                </TableRow>
                        )}
                    </TableBody>
                </Table>
            </Grid>
            <Dialog open={open} onClose={() => setOpen(false)}>
                <DialogTitle>
                    Add Reminder
                </DialogTitle>
                <DialogContent>
                <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    id="description"
                    label="Enter description"
                    name="description"
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    autoFocus
                />
                </DialogContent>
                <DialogActions>
                        <Button onClick={() => setOpen(false)}>Cancel</Button>
                        <Button onClick={submit}>Create Reminder</Button>
                </DialogActions>
            </Dialog>

            <Dialog open={reminderOpen} onClose={() => setReminderOpen(false)}>
                <DialogTitle>
                    Reminder
                </DialogTitle>
                <DialogContent>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Description</TableCell>
                                    <TableCell>{reminder && reminder.description}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell>Status</TableCell>
                                    <TableCell>{reminder && reminder.status ? "Complete" : "Incomplete"}</TableCell>
                                </TableRow>
                            </TableHead>
                        </Table>
                </DialogContent>
                <DialogActions>
                        <Button onClick={() => setReminderOpen(false)}>Close</Button>
                </DialogActions>
            </Dialog>
        </Grid>
    )
}