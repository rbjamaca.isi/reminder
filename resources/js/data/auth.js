export const setToken = (token) => localStorage.setItem('accessToken', token)
export const getToken = () => localStorage.getItem('accessToken')
export const setUserData = (user) => localStorage.setItem('userData', JSON.stringify(user))
export const getUserData = () => JSON.parse(localStorage.getItem('userData'))