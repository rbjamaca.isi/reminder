import axios from 'axios'
import { getToken } from './auth';

export const API_POST = async (data, url) => {
    return await axios.post(url, data, {
        headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + getToken()
        }
    }).then(response => {
        return response;
    });
}

export const API_GET = async (url) => {
    return await axios.get(url, {
        headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + getToken()
        }
    }).then(response => {
        return response;
    });
}

export const API_UPDATE = async (data, url) => {
    return await axios.put(url, data, {
        headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + getToken()
        }
    }).then(response => {
        return response;
    });
}

export const API_DELETE = async (url) => {
    return await axios.delete(url, {
        headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + getToken()
        }
    }).then(response => {
        return response;
    });
}